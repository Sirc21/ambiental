$(".submitform").submit(function(e) {
    e.preventDefault(); 
    var form = $(this);
    var url = form.attr('action');
    var csrftoken = getCookie('csrftoken');
    $.ajax({
        url : url,
        type : "POST",
        data : $(this).serialize(),
        headers:{
            "X-CSRFToken": csrftoken
        },
        success : function(json) {
            //$('#post-text').val(''); // remove the value from the input
            //console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
            swal("successful registration", "You clicked the button!", "success");
        },
        error : function(xhr,errmsg,err) {
            //$('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
               // " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});

$(".submitformformalitie").submit(function(e) {
    e.preventDefault(); 
    var form = $(this);
    var url = form.attr('action');
    var csrftoken = getCookie('csrftoken');
    $.ajax({
        url : url,
        type : "POST",
        data : $(this).serialize(),
        headers:{
            "X-CSRFToken": csrftoken
        },
        success : function(json) {
            //$('#post-text').val(''); // remove the value from the input
            //console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
            swal("successful registration", "You clicked the button!", "success");
            location.href = "/formalites-office";
        },
        error : function(xhr,errmsg,err) {
            //$('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
               // " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});

$(document).on("click",".questionDeleteUser",function(e) {
    e.preventDefault(); 
    let href = $(this).attr('href')
    let rel = $(this).attr('rel')
    swal({
        title: "Esta Seguro de Borrar Este Usuario",
        icon: "warning",
        buttons: {
          cancelar:{
            text:'No, quiero',
            value:'cancel',
            className:'btn btn-danger'
          },
          success:{
            text:'Si, borrar',
            value:'success',
            className:'btn btn-primary'
          },
        },
    })
    .then((value) => {
        switch (value) {
            case "success":
                $.ajax({
                    type: 'POST',
                    url: href,
                    data: {'mydata': JSON.stringify({mcb:rel}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
                    success: function (data, textStatus) {
                        console.log(data)
                        location.href = "/alluser";
                    },
                    error: function(xhr, status, e) {
                        console.log(status, e);
                    }
                });
                return false
            break;

            case "cancel":
                swal.close()
                break;
        }
    });
    return false
});
///////////////////////////////////////////
$(document).on("click",".questionDeleteFormalities",function(e) {
    e.preventDefault(); 
    let href = $(this).attr('href')
    let rel = $(this).attr('rel')
    swal({
        title: "Esta Seguro de Borrar Este Tramite",
        icon: "warning",
        buttons: {
          cancelar:{
            text:'No, quiero',
            value:'cancel',
            className:'btn btn-danger'
          },
          success:{
            text:'Si, borrar',
            value:'success',
            className:'btn btn-primary'
          },
        },
    })
    .then((value) => {
        switch (value) {
            case "success":
                $.ajax({
                    type: 'POST',
                    url: href,
                    data: {'mydata': JSON.stringify({mcb:rel}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
                    success: function (data, textStatus) {
                        console.log(data)
                        location.href = "/allformalities";
                    },
                    error: function(xhr, status, e) {
                        console.log(status, e);
                    }
                });
                return false
            break;

            case "cancel":
                swal.close()
                break;
        }
    });
    return false
});
//////////////////////////////////////////////////////////////ofidelete
$(document).on("click",".questionDeleteOffice",function(e) {
    e.preventDefault(); 
    let href = $(this).attr('href')
    let rel = $(this).attr('rel')
    swal({
        title: "Esta Seguro de Borrar Esta Oficina",
        icon: "warning",
        buttons: {
          cancelar:{
            text:'No, quiero',
            value:'cancel',
            className:'btn btn-danger'
          },
          success:{
            text:'Si, borrar',
            value:'success',
            className:'btn btn-primary'
          },
        },
    })
    .then((value) => {
        switch (value) {
            case "success":
                $.ajax({
                    type: 'POST',
                    url: href,
                    data: {'mydata': JSON.stringify({mcb:rel}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
                    success: function (data, textStatus) {
                        console.log(data)
                        location.href = "/alloffices";
                    },
                    error: function(xhr, status, e) {
                        console.log(status, e);
                    }
                });
                return false
            break;

            case "cancel":
                swal.close()
                break;
        }
    });
    return false
});
/////////////////////////////////////////////
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
            if (elem.requestFullScreen) {
                    elem.requestFullScreen();
                } else if (elem.mozRequestFullScreen) {
                    elem.mozRequestFullScreen();
                } else if (elem.webkitRequestFullScreen) {
                    elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                } else if (elem.msRequestFullscreen) {
                    elem.msRequestFullscreen();
                }
            }
            else {
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
            }
        }
////////////////////////////////////////////////////
let num = 0
process = []
$(document).on("click",".addProcess",function() {
    num = num + 1
    $.ajax({
        url: '/oficces-enable',
        //data: {'username': username},
        dataType: 'json',
        success: function (request) {
            let oficces = ''
            request.data.forEach(function(element) {
                oficces = oficces+'<option value="'+element.id+'">'+element.name+'</option>'
            });
            $('#tableProcess tbody tr:last')
            .after(
                '<tr class="process'+num+'" id="'+num+'">'+
                    '<td>'+num+'</td>'+
                    '<td><input type="text" id="nameProcess" name="nameProcess" class="nameProcess form-control col-md-7 col-xs-12"></td>'+
                    '<td>'+
                        '<div class="form-group">'+
                            '<div class="col-md-9 col-sm-9 col-xs-12">'+
                            '<select name="office" class="form-control office">'+
                                '<option selected="true" disabled="disabled">Seleccione una oficina</option>  '+
                                 oficces+
                            '</select>'+
                            '</div>'+
                        '</div>'+
                    '</td>'+
                    '<td id="process'+num+'Requeriments" width="200px">'+
                    '</td>'+
                    '<td>'+
                        '<div class="form-group">'+
                            '<div class="col-md-9 col-sm-9 col-xs-12">'+
                            '<select name="return" class="form-control return">'+
                                '<option selected="true" disabled="disabled">Seleccione una oficina</option>  '+
                                 oficces+
                            '</select>'+
                            '</div>'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        //'<button type="button" class="btn btn-primary btnAddRequeriments" data-toggle="modal" data-target=".bs-addRequeriments-modal-sm">add Requisito</button>'+
                        '<button type="button" class="btn btn-primary btnAddRequeriments" data-toggle="modal" data-target=".bs-addRequeriments-modal-lg">add Requisito</button>'+
                        '<button type="button" class="btn btn-primary">Eliminar Proceso</button>'+
                    '</td>'+
                '</tr>'
            );

            
            addprocess = {
                number:num,
                processName:'',
                process:'process'+num,
                oficce:'',
                return:'',
                requirements:[],
            }
            process.push(addprocess)

        }
    });
});


$(document).on("change",".office",function() {
    var office_id = $(this).find("option:selected").attr("value");
    let process_name = $(this).closest( "tr" ).attr('class')
    search(process_name,process).oficce = parseInt(office_id)
});

$(document).on("change",".return",function() {
    var office_id = $(this).find("option:selected").attr("value");
    let process_name = $(this).closest( "tr" ).attr('class')
    search(process_name,process).return = parseInt(office_id)
});

$(document).on("keyup",".nameProcess",function() {
    let process_name = $(this).closest( "tr" ).attr('class')
    search(process_name,process).processName = $(this).val()
});


$(document).on("click",".btnAddRequeriments",function() {
    let process_name = $(this).closest( "tr" ).attr('class')
    $.ajax({
        url: '/requeriments-enable',
        dataType: 'json',
        success: function (request) {
            /*console.log(request)
            $('#addRequeriments').find('option').remove().end()
            .append('<option selected="true" disabled="disabled">Seleccione un requisito</option>')

            request.data.forEach(function(element) {
                $("#addRequeriments").append(new Option(element.name, element.id+'|'+element.name+'|'+process_name));
            });*/
            $('#tableRequeriments > tbody').html('')
            request.data.forEach(function(element){
                $('#tableRequeriments > tbody:last-child').append(
                    '<tr>'+
                        '<td>'+element.name+'</td>'+
                        '<td><input type="checkbox" class="classRequeriments" name="requerimentsSave" value="'+element.id+'|'+element.name+'|'+process_name+'"></td>'+
                    '</tr>'
                );
            })
            $('#tableRequeriments > tbody:last-child').append(
                '<tr class="notfound">'+
                '<td colspan="2">No se encuentro nada parecido</td>'+
                '</tr>'
            );
        }
    });
});

$(document).on("click","#resetRequeriments",function() {
    $('input:checkbox').removeAttr('checked');
})
$(document).on("click","#saveRequeriments",function() {
    $("input:checkbox[name=requerimentsSave]:checked").each(function(){
        let value = $(this).val()
        let requeriment_id = value.split("|")[0];
        let requeriment_name = value.split("|")[1];
        let process_name = value.split("|")[2];
        search(process_name,process).requirements.push(parseInt(requeriment_id))
        $('#'+process_name+'Requeriments').append('<div class="dialog-tag">'+requeriment_name+'<a href="#" class="close-thik-tag"></a></div>');
    });
    $('.bs-addRequeriments-modal-lg').modal('toggle');
    $('input:checkbox').removeAttr('checked');
});
///////busqueda en tabla//////
$(document).on("keyup","#txt_searchall",function() {
    var search = $(this).val();
    $('#tableRequeriments tbody tr').hide();
    var len = $('#tableRequeriments tbody tr:not(.notfound) td:nth-child(1):contains("'+search+'")').length;
    if(len > 0){
      $('#tableRequeriments tbody tr:not(.notfound) td:contains("'+search+'")').each(function(){
        $(this).closest('tr').show();
      });
    }else{
      $('.notfound').show();
    }
});
//////////////////////////////
/*$(document).on("change","#addRequeriments",function() {
    var value = $(this).find("option:selected").attr("value");
    let requeriment_id = value.split("|")[0];
    let requeriment_name = value.split("|")[1];
    let process_name = value.split("|")[2];
    search(process_name,process).requirements.push(parseInt(requeriment_id))
    $('.bs-addRequeriments-modal-sm').modal('toggle');
    $('#'+process_name+'Requeriments').append('<div class="dialog-tag">'+requeriment_name+'<a href="#" class="close-thik-tag"></a></div>');
});*/

$(document).on("click",".send",function(e) {
    let nameformalitie = $('#nameFormalitie').val()
    let form = {
        nameFormalitie:nameformalitie,
        process:process
    }
    //console.log(form)
    //return false
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/create-ajax-formalities',
        data: {'mydata': JSON.stringify(form), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
        success: function (data, textStatus) {
            console.log(data)
        },
        error: function(xhr, status, e) {
            console.log(status, e);
        }
    });
});

function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].process === nameKey) {
            return myArray[i];
        }
    }
}
////////////////////////////////////////////////////
$(document).on("click",".tracing",function(e) {
    e.preventDefault();
    let tracing = $(this).attr('rel')
    let route = $(this).attr('href')
    $.ajax({
        type: 'POST',
        url: route,
        data: {'mydata': JSON.stringify({mcb:tracing}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
        success: function (data, textStatus) {
            $('#x_content_formalities').html('')
            $('#x_content_formalities').html(data)
        },
        error: function(xhr, status, e) {
            console.log(status, e);
        }
    });
    return false
});

$(document).on("click",".tracing_not",function(e) {
    e.preventDefault();
    let tracing = $(this).attr('rel')
    let route = $(this).attr('href')
    $.ajax({
        type: 'POST',
        url: route,
        data: {'mydata': JSON.stringify({mcb:tracing}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
        success: function (data, textStatus) {
            $('#x_content_formalities_not').html('')
            $('#x_content_formalities_not').html(data)
        },
        error: function(xhr, status, e) {
            console.log(status, e);
        }
    });
    return false
});

$(document).on("click",".runTracing",function(e) {
    e.preventDefault();
    let tracing = $(this).attr('rel')
    let route = $(this).attr('href')
    let requeriments = []
    $("input:checkbox[name=requeriments]:checked").each(function(){
        requeriments.push($(this).val());
    });
    selectedUser = $('#assignUsers').children("option:selected").val();
    $.ajax({
        type: 'POST',
        url: route,
        data: {'mydata': JSON.stringify({mcb:tracing,selectedUser:selectedUser,requeriments:requeriments}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
        success: function (data, textStatus) {
            //$('#x_content_formalities').html('')
            //$('#x_content_formalities').html(data)
            location.href = "/run-formalities-office";
        },
        error: function(xhr, status, e) {
            console.log(status, e);
        }
    });
    return false
});

$(document).on("click",".continueTracing",function(e) {
    e.preventDefault();

    swal({
        title: "Esta Seguro de recibir este Tramite",
        icon: "warning",
        buttons: {
          cancelar:{
            text:'No, quiero',
            value:'cancel',
            className:'btn btn-danger'
          },
          success:{
            text:'Si, Recibir',
            value:'success',
            className:'btn btn-primary'
          },
        },
    })
    .then((value) => {
        switch (value) {
            case "success":
                let tracing = $(this).attr('rel')
                let route = $(this).attr('href')
                $.ajax({
                    type: 'POST',
                    url: route,
                    data: {'mydata': JSON.stringify({mcb:tracing}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
                    success: function (data, textStatus) {
                        $('#x_continue_formalities').html('')
                        $('#x_continue_formalities').html(data)
                        //location.href = "/run-formalities-office";
                    },
                    error: function(xhr, status, e) {
                        console.log(status, e);
                    }
                });
                return false
            break;

            case "cancel":
                swal.close()
                break;
        }
    });

    return false
});

$(document).on("click",".finishTracing",function(e) {
    e.preventDefault();

    swal({
        title: "Esta Seguro de Finalizar este Tramite",
        icon: "warning",
        buttons: {
          cancelar:{
            text:'No, quiero',
            value:'cancel',
            className:'btn btn-danger'
          },
          success:{
            text:'Si, Finalizar',
            value:'success',
            className:'btn btn-primary'
          },
        },
    })
    .then((value) => {
        switch (value) {
            case "success":
                let tracing = $(this).attr('rel')
                let route = $(this).attr('href')
                $.ajax({
                    type: 'POST',
                    url: route,
                    data: {'mydata': JSON.stringify({mcb:tracing}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
                    success: function (data, textStatus) {
                        //$('#x_continue_formalities').html('')
                        //$('#x_continue_formalities').html(data)
                        location.href = "/run-formalities-office";
                    },
                    error: function(xhr, status, e) {
                        console.log(status, e);
                    }
                });
                return false
            break;

            case "cancel":
                swal.close()
                break;
        }
    });

    return false
});

$(document).on("click",".tracingStatus",function(e) {
    e.preventDefault();
    let tracing = $(this).attr('rel')
    let route = $(this).attr('href')
    $.ajax({
        type: 'POST',
        url: route,
        data: {'mydata': JSON.stringify({mcb:tracing}), 'csrfmiddlewaretoken':  getCookie('csrftoken')},
        success: function (data, textStatus) {
            console.log(data)
            $('#x_content_tracingStatus').html('')
            $('#x_content_tracingStatus').html(data)
        },
        error: function(xhr, status, e) {
            console.log(status, e);
        }
    });
    return false
});
/////////////////////////////////////////////////////////////////////////
$("#id_buscar").keyup(function(e){
    var secred=$("#id_buscar").val();
    $.ajax({
      data: {'secre': secre},
      url: '/viaticos/buscarSecre/',
      type: 'get',
      success : function(data) {
        var html="";
        console.log(data.length);
        $.each(data, function(index, objeto){
            val=""
            if (objeto.montosobrante<0){
              val="Excedido"
            }
            else{
              val=objeto.montosobrante
            }
            html +='<tr >';
            html +='<td>'+objeto.numero+'</td>'      
            html +='<td>'+objeto.secre+'</td>'
            html +='<td>'+objeto.MontoGasto+'</td>'
            html +='<td>'+ 
              '<div class="progress progress_sm">'+
                '<div class="progress-bars bg-green" data-transitiongoal="'+objeto.barra+" aria-valuenow="+objeto.barra+'" style="background-color:'+objeto.color+';width: '+objeto.barra+'%;"></div>'+
              '</div>'+
              '<small>'+ objeto.barra+'% Complete</small>'+'</td>'    
            html +='<td>'+val+'</td>'
            html +='<td>'+objeto.SaldoSecre+'</td>'
            html +='</tr>';          
        });
        $('#tags1').html(html);
      },
      error : function(data) {
        //console.log(data);
      } 
    });

  });