$(document).on("submit",".sendreport",function(e) {
    e.preventDefault(); 
    var form = $(this);
    var url = form.attr('action');
    var csrftoken = getCookie('csrftoken');
    console.log(form)
    alert(url)
    alert(csrftoken)
    //return false
    $.ajax({
        url : url,
        type : "POST",
        data : $(this).serialize(),
        headers:{
            "X-CSRFToken": csrftoken
        },
        success : function(json) {
            //creamos el reporte que nos llega desde django
            var blob=new Blob([json]);
            var link=document.createElement('a');
            link.setAttribute("target", "_blank");
            link.href=window.URL.createObjectURL(blob);
            link.download="rerport.pdf"; // y le damos un nombre aqui cambialo a lo que tu quieras
            link.click();
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
    return false
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}