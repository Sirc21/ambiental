# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.http import JsonResponse
from django.db.models import Q
#from pprint import pprint
from .models import Offices
from .forms import *
import json

def viewCreateOficce(request):
    return render(request,'offices/createOficce.html')

def createOfficce(request):
    if request.method == 'POST':
        form = formCreateOffices(request.POST)
        if form.is_valid():
            nameOffice = form.cleaned_data['name']
            newOffice = Offices(name=nameOffice)
            newOffice.save()
            return render(request,"offices/createOficce.html",{'form':form})
    else:
        form = formCreateOffices()
    return render(request,"offices/createOficce.html",{'form':form})

def allOfficceEnable(request):
    office = Offices.objects.filter(state='ENABLE')
    response = []
    for val in office:
        response.append({
            'id':val.id,
            'name':val.name
        })
    #print(response)
    return JsonResponse({'data':response})

def officeView(request):
    office = Offices.objects.all()
    contexto = {"offices":office}
    return render(request,'offices/seeOffices.html', contexto)

def deleteOffices(request):
    if request.method == 'POST':
        if request.is_ajax():
            print "**ajax post**"
            data = request.POST['mydata']
            astr = data
            d = json.loads(data)
            print d
            instance = Offices.objects.get(id=int(d['mcb']))
            instance.delete()
            return HttpResponse(astr)
    return render(request)
