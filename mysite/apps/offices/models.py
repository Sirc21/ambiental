# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

class Offices(models.Model):
    name = models.CharField(max_length=200)
    state = models.CharField(max_length=200,default='ENABLE')
    created_date = models.DateTimeField(default=timezone.now)
    def saveOffices(self):
        self.save()

    def __str__(self):
        return self.name
    