from django.conf.urls import * 
from .views import *
from django.contrib.auth.views import login, logout, LogoutView
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views

app_name= 'offices'
urlpatterns = [
    url(r'^register-oficces',createOfficce),
    url(r'^oficces-enable',allOfficceEnable),
    url(r'^alloffices',login_required(officeView), name='alloffices'),
    url(r'^deleteoffice',login_required(deleteOffices), name='deleteoffice'),
    #url(r'^nuevo$', OfficesCreation.as_view(), name='new'),
]
