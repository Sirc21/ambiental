from django import forms
#from django.contrib.auth.models import User
#from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser
from mysite.apps.offices.models import Offices
#
class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email')

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email')
#

class loginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30,widget=forms.TextInput(attrs={'id' : 'username','autocomplete':'off','placeholder':'Usuario'}))
    password = forms.CharField(label="Password", max_length=30,widget=forms.PasswordInput(attrs={'id' : 'password','placeholder':'Password'}))

#class registrationForm(UserCreationForm):
class registrationForm(CustomUserCreationForm):
    #email = forms.EmailField(required=True)
    office = Offices.objects.all()
    result = []
    for item in office:
        result.append((item.id, item.name))
    email = forms.CharField(label="email",widget=forms.TextInput(attrs={'class':'form-control','id' : 'email','autocomplete':'off'}))
    first_name = forms.CharField(label="last_name",widget=forms.TextInput(attrs={'class':'form-control','id' : 'last_name','autocomplete':'off'}))
    last_name = forms.CharField(label="last_name",widget=forms.TextInput(attrs={'class':'form-control','id' : 'last_name','autocomplete':'off'}))
    email = forms.CharField(label="email",widget=forms.TextInput(attrs={'class':'form-control','id' : 'email','autocomplete':'off'}))
    username = forms.CharField(label="username",widget=forms.TextInput(attrs={'class':'form-control','id' : 'username','autocomplete':'off'}))
    #office = forms.CharField(label="office",widget=forms.TextInput(attrs={'class':'form-control','id' : 'oficina','autocomplete':'off'}))
    office = forms.ChoiceField(choices=result,widget=forms.Select(attrs={'class':'form-control','id' : 'username','autocomplete':'off'}))
    password1 = forms.CharField(label="password1",widget=forms.PasswordInput(attrs={'class':'form-control','id' : 'password1','autocomplete':'off'}))
    password2 = forms.CharField(label="password2",widget=forms.PasswordInput(attrs={'class':'form-control','id' : 'password2','autocomplete':'off'}))

    #class Meta:
        #model = User
        #fields = (
            #'username',
            #'first_name',
            #'last_name',
            #'email',
            #'password1', 
            #'password2',
        #)
    
    def save(self,commit=True):
        user=super(registrationForm,self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.office = self.cleaned_data['office']

        if commit:
            user.save()

        return user

#class AuthenticationForm(forms.Form): # aqui estaba mal es... asi forms.Form no asi forms.form
    #username = forms.CharField(required=True)
    #password = forms.CharField(label=_("password"), widget=forms.PasswordInput)