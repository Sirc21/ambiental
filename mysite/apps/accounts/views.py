# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth import login
from django.contrib.auth import get_user_model
from django.db import connection
User = get_user_model()
from mysite.apps.offices.models import Offices
from django.contrib.auth.decorators import login_required
import json
from .forms import *
# Create your views here.

def signupViewssss(request):
    return render(request,'accounts/signup.html')

def signupView(request):
    if request.method == 'POST':
        form = registrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/signup')
            #return render(request,'accounts/signup.html',context_instance=RequestContext(request))
        else:
            print form

    else:
        form = registrationForm()
        #args = {'form':form}
    return render(request,'accounts/signup.html',{'form':form})

def signinView(request):
    if request.method =='POST':
        form = loginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request,user)
            return redirect('/home')
    else:
        form = loginForm()
    return render(request,'accounts/signin.html',{'form':form})

def logoutView(request):
    if request.method =='POST':
        logout(request)
    form = loginForm() 
    return render(request,'accounts/signin.html',{'form':form})

def homeView(request):
    return render(request,'main/index_gentella.html')

def offices(request):
    if hasattr(request.user, 'office'):
        office = Offices.objects.filter(id=request.user.office)
        return {'officeUser':office[0]}
    else:
        return {'officeUser':'none'}

def alluserView(request):
    with connection.cursor() as cursor:
        cursor.execute("select * from get_users_office()")
        rowusers = dictfetchall(cursor)
        contexto = {"accounts":rowusers}
        return render(request,"accounts/alluser.html",contexto)

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
def deleteUsers(request):
    if request.method == 'POST':
        if request.is_ajax():
            print "**ajax post**"
            data = request.POST['mydata']
            astr = data
            d = json.loads(data)
            print d
            instance = User.objects.get(id=int(d['mcb']))
            instance.delete()
            return HttpResponse(astr)
    return render(request)
#class userDelete(Deleteview):
#    model=users
#    template_name='account/userDelete.html'
#    success_url = reverse_lazy('account:users')
