from django.conf.urls import * 
from .views import *
from django.contrib.auth.views import login, logout, LogoutView
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views

app_name= 'accounts'
urlpatterns = [
    url(r'^signup',login_required(signupView), name='signup'),
    url(r'^$',signinView),
    #url(r'^logout',auth_views.LogoutView.as_view(), name='logout',),
    url(r'^logout', logout, name="salir", kwargs={'next_page': 'http://localhost:8000/'}),
    url(r'^home',login_required(homeView), name='home'),#creamos esta ruta para luego que inicie session a dond le re direcionara
    url(r'^alluser',login_required(alluserView), name='alluser'),
    url(r'^deleteuser',login_required(deleteUsers), name='deleteuser'),
]