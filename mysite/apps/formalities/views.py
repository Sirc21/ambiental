# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.http import JsonResponse
from django.db.models import Q
from django.db import connection
from .models import Formalities,Requirements,Processes,RequirementsFormalities,registryFormalities,registryFormalitiesRequeriments
from mysite.apps.offices.models import Offices
from mysite.apps.workflow.models import Workflow
from passlib.hash import pbkdf2_sha256
from datetime import datetime
import json
from .forms import *

def viewCreateFormalities(request):
    return render(request,'formalities/createFormalities.html')

def allRequerimentsEnable(request):
    requirements = Requirements.objects.filter(state='ENABLE')
    response = []
    for val in requirements:
        response.append({
            'id':val.id,
            'name':val.name
        })
    #print(response)
    return JsonResponse({'data':response})

def createAjaxFormalities(request):
    if request.method == 'POST':
        if request.is_ajax():
            print "**ajax post**"
            data = request.POST['mydata']
            astr = data
            d = json.loads(data)
            newFormalitie = Formalities(name=d['nameFormalitie'])
            newFormalitie.save()
            #print d['process']
            #print newFormalitie.id
            for f in d['process']:
                newProcess = Processes(name=f['processName'],formalitie_id=newFormalitie.id,office_id=f['oficce'],order_process=f['number'],return_office_id=f['return'])
                newProcess.save()
                for r in f['requirements']:
                    newRequeriments = RequirementsFormalities(formalitie_id=newFormalitie.id,requirement_id=r,process_id=newProcess.id)
                    newRequeriments.save()
            #print 'Guardado'
            return HttpResponse(astr)
    return render(request)

def formalitiesOffice (request):
    if request.user.is_authenticated():
        office_user = request.user.office
        with connection.cursor() as cursor:
            cursor.execute("select * from get_all_formalities_user_office(%s)", [office_user])
            row = dictfetchall(cursor)
            office = Offices.objects.filter(id=office_user)
            return render(request, 'formalities/startFormalities.html',{'formalities':row,'office':office[0].name})

def startFormalities (request,id_formalitie):
    form = formRegisterFormalitie()
    with connection.cursor() as cursor:
            cursor.execute("select * from get_requeriments_formalitie(%s)", [id_formalitie])
            rowrequerimentes = dictfetchall(cursor)
            formalitie = Formalities.objects.filter(id=id_formalitie)

            cursor.execute("select * from get_users_next_process(%s,%s)", [id_formalitie,1])
            rowusers = dictfetchall(cursor)
            return render(request,"formalities/registerFormalitie.html",{'form':form,'requeriments':rowrequerimentes,'id_formalitie':id_formalitie,'formalitie':formalitie[0],'users':rowusers})

def postStartFormalities(request):
    if request.method == 'POST':
        form = formRegisterFormalitie(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            reference = form.cleaned_data['reference']
            routeNumber = form.cleaned_data['routeNumber']
            origin = form.cleaned_data['origin']
            datereceived = form.cleaned_data['datereceived']
            rmts = request.POST.getlist('requeriments[]')
            formalitie = request.POST['mcb']
            #assign = request.POST['assign']
            with connection.cursor() as cursor:
                createhash = pbkdf2_sha256.encrypt(title+reference+routeNumber+str(origin)+datereceived+formalitie+str(datetime.today()),rounds=12000,salt_size=32)
                newRegistry = registryFormalities(title=title,reference=reference,route_number=int(routeNumber),origin=origin,datereceived=datereceived,formalitie_id=formalitie,office_id=request.user.office,formalitie_hash=createhash,username = request.user.username)
                newRegistry.save()
                cursor.execute("select * from get_requeriments_formalitie(%s)", [formalitie])
                rowrequerimentes = dictfetchall(cursor)
                for rq in rowrequerimentes:
                    newRfr = registryFormalitiesRequeriments(formalitie_hash=createhash,requirements_id=int(rq['r_id']))
                    newRfr.save()

                for rm in rmts:
                    updateRfr = registryFormalitiesRequeriments.objects.filter(formalitie_hash=createhash,requirements_id=rm)
                    updateRfr.update(state=True)
                    
                #cursor.execute("select * from get_first_process_formalitie(%s)",[formalitie])
                #row = dictfetchall(cursor)
                #newFirstProcess = Workflow(formalitie_hash=createhash,office_id=row[0]['r_office_id'],order_hash=row[0]['r_order_process'],tracing=1,state_officie='VERIFIED',user_id=request.user.id,user_assign=request.user.id,)
                #newFirstProcess.save()

                #cursor.execute("select * from continue_process_formalitie(%s)", [createhash])
                #rowsend = dictfetchall(cursor)
                #print rowsend
                #if rowsend:
                #        newFirstProcess = Workflow(formalitie_hash=createhash,office_id=rowsend[0]['r_office'],order_hash=row[0]['r_order_process'],tracing=2,state='ENVIANDO',user_id=request.user.id,user_assign=int(assign))
                #        newFirstProcess.save()
                #        print 'GUARDADO'
                return render(request,"formalities/registerFormalitie.html",{'form':form})
    else:
        form = formRegisterFormalitie()
    return render(request,"formalities/registerFormalitie.html",{'form':form})

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def viewRegistro (request):
    return render(request, 'formalities/registro.html')

def createRequirements(request):
    if request.method == 'POST':
        form = formCreateRequirements(request.POST)
        if form.is_valid():
            nameRequirements = form.cleaned_data['name']
            newRequirements = Requirements(name=nameRequirements)
            newRequirements.save()
            return render(request,'formalities/createRequirements.html',{'form':form})
    else:
        form = formCreateRequirements()
    return render(request,'formalities/createRequirements.html',{'form':form})

def formalitiesView(request):
    formalitie = Formalities.objects.all()
    contexto = {"formalities":formalitie}
    return render(request,'formalities/seeFormalities.html', contexto)

def deleteFormalities(request):
    if request.method == 'POST':
        if request.is_ajax():
            print "**ajax post**"
            data = request.POST['mydata']
            astr = data
            d = json.loads(data)
            print d
            instance = Formalities.objects.get(id=int(d['mcb']))
            instance.delete()
            return HttpResponse(astr)
    return render(request)

def viewRequirementsFormalities (request):
    return render(request, 'formalities/seeFormalitiesRequirements.html')
