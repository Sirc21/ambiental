from django.conf.urls import *
from .views import *
urlpatterns = [
    url(r'^register-formalities',viewCreateFormalities),
    url(r'^create-ajax-formalities',createAjaxFormalities),
    url(r'^requeriments-enable',allRequerimentsEnable),
    url(r'^formalites-office',formalitiesOffice),
    url(r'^register-formalitie/(?P<id_formalitie>\w{0,50})/$',startFormalities),
    url(r'^post-start-formalites',postStartFormalities),
    url(r'^create-requirements',createRequirements),
    url(r'^registro',viewRegistro),
    url(r'^allformalities',formalitiesView),
    url(r'^deleteformalities',deleteFormalities),
    url(r'^seeFormalitiesRequeriments',viewRequirementsFormalities),
    #url(r'^nuevo$',OfficesCreation.as_view(), name='new'),
]