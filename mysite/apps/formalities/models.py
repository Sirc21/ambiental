# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

class Formalities(models.Model):
    name = models.CharField(max_length=200)
    state = models.CharField(max_length=200,default='ENABLE')
    created_date = models.DateTimeField(
            default=timezone.now)

    def saveFormalities(self):
        self.save()

class Requirements(models.Model):
    name = models.CharField(max_length=300)
    state = models.CharField(max_length=200,default='ENABLE')
    created_date = models.DateTimeField(
            default=timezone.now)

    def saveRequirements(self):
        self.save()

class registryFormalitiesRequeriments(models.Model):
    formalitie_hash = models.CharField(max_length=200)
    requirements_id = models.IntegerField()
    state = models.BooleanField(default=False)

    def saveRegistryFormalitiesRequeriments(self):
        self.save()

class registryFormalities(models.Model):
    title = models.CharField(max_length=200)
    reference = models.CharField(max_length=200)
    route_number = models.IntegerField()
    origin = models.CharField(max_length=200)
    datereceived = models.CharField(max_length=200)
    formalitie_id = models.IntegerField()
    office_id = models.IntegerField()
    formalitie_hash = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    created_date = models.DateTimeField(
            default=timezone.now)

    def saveRequirements(self):
        self.save()

class RequirementsFormalities(models.Model):
    formalitie_id = models.IntegerField()
    requirement_id = models.IntegerField()
    process_id = models.IntegerField()
    state = models.CharField(max_length=200,default='ENABLE')
    created_date = models.DateTimeField(
            default=timezone.now)

    def saveRequirements(self):
        self.save()

class Processes(models.Model):
    name = models.CharField(max_length=200)
    formalitie_id = models.IntegerField()
    office_id = models.IntegerField()
    order_process = models.IntegerField()
    return_office_id = models.IntegerField()
    created_date = models.DateTimeField(
            default=timezone.now)

    def saveProcesses(self):
        self.save()
