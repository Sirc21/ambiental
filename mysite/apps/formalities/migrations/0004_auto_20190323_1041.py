# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2019-03-23 14:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('formalities', '0003_processes_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='processes',
            old_name='order',
            new_name='order_process',
        ),
    ]
