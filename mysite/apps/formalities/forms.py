from django import forms
from .models import *
class formCreateRequirements(forms.Form):
    name = forms.CharField(label="nombre", max_length=300,widget=forms.TextInput(attrs={'id':'first-name','required':'required','class':'form-control col-md-7 col-xs-12'}))

class formRegisterFormalitie(forms.Form):
    title = forms.CharField(label="title", max_length=30,widget=forms.TextInput(attrs={'id':'title','required':'required','class':'form-control col-md-7 col-xs-12'}))
    reference = forms.CharField(label="Referencia", max_length=30,widget=forms.TextInput(attrs={'id':'reference','required':'required','class':'form-control col-md-7 col-xs-12'}))
    routeNumber = forms.CharField(label="Numero de hoja de Ruta", max_length=30,widget=forms.TextInput(attrs={'id':'routeNumber','required':'required','class':'form-control col-md-7 col-xs-12'}))
    origin = forms.CharField(label="Procedencia", max_length=30,widget=forms.TextInput(attrs={'id':'origin','required':'required','class':'form-control col-md-7 col-xs-12'}))
    datereceived = forms.CharField(label="Fecha de Recibido", max_length=30,widget=forms.TextInput(attrs={'id':'datereceived','required':'required','class':'form-control col-md-7 col-xs-12'}))
