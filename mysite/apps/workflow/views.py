# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,HttpResponse
from django.db import connection
from mysite.apps.offices.models import Offices
from .models import Workflow
import json

from reportlab.pdfgen import canvas
from django.http import HttpResponse

def RunformalitiesOffice (request):
        with connection.cursor() as cursor:
                try:
                        office = Offices.objects.filter(id=request.user.office)

                        cursor.execute("select * from get_register_office_start(%s)", [request.user.office])
                        row = dictfetchall(cursor)
                        
                        cursor.execute("select * from get_formalities_office_start_recevied(%s,%s,%s,%s)", [request.user.office,request.user.id,'ENTREGADO','NONE'])
                        rowsend = dictfetchall(cursor)

                        cursor.execute("select * from get_formalities_office_start_recevied(%s,%s,%s,%s)", [request.user.office,request.user.id,'INICIADO','VERIFIED'])
                        rowstart = dictfetchall(cursor)

                        cursor.execute("select * from get_formalities_office_start_recevied(%s,%s,%s,%s)", [request.user.office,request.user.id,'FINALIZADO','VERIFIED'])
                        rowfinish = dictfetchall(cursor)

                        cursor.execute("select * from get_formalities_office_not_start(%s)", [request.user.office])
                        rownotstart = dictfetchall(cursor)

                        return render(request, 'workflow/formalities_workflow.html',{
                                'formalities':row,
                                'formalitiesSend':rowsend,
                                'formalitiesStart':rowstart,
                                'formalitiesFinish':rowfinish,
                                'formalitiesNotStart':rownotstart,
                                'office':office[0].name
                                })
                finally:
                        cursor.close()

def startProcessFormalitie(request):
    if request.method == 'POST':
        if request.is_ajax():
                print "**ajax post**"
                data = request.POST['mydata']
                astr = data
                d = json.loads(data)
                with connection.cursor() as cursor:
                        try:
                                office_user = request.user.office
                                #print d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$")
                                #cursor.execute("select * from get_last_process_workflow_update(%s,%s)",[d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$"),'VERIFIED']) 
                                print d['mcb']
                                cursor.execute("select * from start_formalitie_check(%s)", [d['mcb']])
                                row = dictfetchall(cursor)
                                #print row
                                if row:
                                        #newFirstProcess = Workflow(formalitie_hash=d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$"),office_id=request.user.office,order_hash=row[0]['r_order_process'],tracing=row[0]['r_tracing'],state='RECIBIDO',user_id=request.user.id,user_assign=request.user.id)
                                        #newFirstProcess.save()
                                        #office = Offices.objects.filter(id=office_user)
                                        return render(request, 'workflow/continue_workflow.html',{'formalities':row})
                        finally:
                                cursor.close()
    return render(request)

def sendProcessFormalitie(request):
    if request.method == 'POST':
        if request.is_ajax():
                print "**ajax post**"
                data = request.POST['mydata']
                astr = data
                d = json.loads(data)
                print d
                with connection.cursor() as cursor:
                        try:
                                office_user = request.user.office
                                cursor.execute("select * from start_formalitie_check(%s)",[d['mcb']])
                                row = dictfetchall(cursor)
                                
                                if row:
                                        newFirstProcess = Workflow(formalitie_hash=d['mcb'],office_id=row[0]['r_office'],order_hash=row[0]['r_order_process'],tracing=row[0]['r_tracing'],state='INICIADO',state_officie='VERIFIED',user_id=request.user.id,user_assign=request.user.id,)
                                        newFirstProcess.save()
                                        cursor.execute("select * from continue_process_formalitie(%s)", [d['mcb']])
                                        row = dictfetchall(cursor)
                                        if row:
                                                newFirstProcess = Workflow(formalitie_hash=d['mcb'],office_id=row[0]['r_office'],order_hash=row[0]['r_order_process'],tracing=1,state='ENTREGADO',user_id=request.user.id,user_assign=d['selectedUser'])
                                                newFirstProcess.save()
                                                #newFirstProcess = Workflow(formalitie_hash=d['mcb'],office_id=row[0]['r_office'],order_hash=row[0]['r_order_process'],state='RECIBIDO',user_id=request.user.id)
                                                #newFirstProcess.save()
                                                #print 'ENVIANDO'
                                                #office = Offices.objects.filter(id=office_user)
                                                #return render(request, 'workflow/continue_workflow.html',{'formalities':row,'office':office[0].name})
                                                return HttpResponse('sucess')
                        finally:
                                cursor.close()
    return render(request)

def continueTracingFormalitie(request):
    if request.method == 'POST':
        if request.is_ajax():
                print "**ajax post**"
                data = request.POST['mydata']
                astr = data
                d = json.loads(data)
                with connection.cursor() as cursor:
                        try:
                                office_user = request.user.office
                                #esto esta comentado par aque sigas haciendo la prueba
                                #print d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$")
                                #cursor.execute("select * from get_last_process_workflow_update(%s,%s)",[d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$"),'VERIFIED']) 
                                #print d['mcb']
                                #esto esta comentado par aque sigas haciendo la prueba
                                cursor.execute("select * from continue_process_formalitie(%s)", [d['mcb']])
                                row = dictfetchall(cursor)
                                print row
                                if row:
                                        #esto esta comentado par aque sigas haciendo la prueba
                                        #newProcess = Workflow(formalitie_hash=d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$"),office_id=request.user.office,order_hash=row[0]['r_order_process'],tracing=row[0]['r_tracing']+1,state='RECIBIDO',state_officie='VERIFIED',user_id=request.user.id,user_assign=request.user.id)
                                        #newProcess.save()
                                        #office = Offices.objects.filter(id=office_user)
                                        #esto esta comentado par aque sigas haciendo la prueba
                                        return render(request, 'workflow/continue_workflow.html',{'formalities':row})
                        finally:
                                cursor.close()
    return render(request)

def finishTracingFormalitie(request):
    if request.method == 'POST':
        if request.is_ajax():
                print "**ajax post**"
                data = request.POST['mydata']
                astr = data
                d = json.loads(data)
                with connection.cursor() as cursor:
                        try:
                                office_user = request.user.office
                                #print d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$")
                                #cursor.execute("select * from get_last_process_workflow_update(%s,%s)",[d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$"),'VERIFIED']) 
                                #print d['mcb']
                                cursor.execute("select * from continue_process_formalitie(%s)", [d['mcb']])
                                row = dictfetchall(cursor)
                                print row
                                if row:
                                        newProcess = Workflow(formalitie_hash=d['mcb'].replace("ccformalitiecc", "$pbkdf2-sha256$12000$"),office_id=request.user.office,order_hash=row[0]['r_order_process'],tracing=row[0]['r_tracing']+1,state='FINALIZADO',state_officie='VERIFIED',user_id=request.user.id,user_assign=request.user.id)
                                        newProcess.save()
                                        #office = Offices.objects.filter(id=office_user)
                                        #return render(request, 'workflow/continue_workflow.html',{'formalities':row})
                                        return HttpResponse('FINISH')
                        finally:
                                cursor.close()
    return render(request)

def statusTracingFormalitie(request):
    if request.method == 'POST':
        if request.is_ajax():
                print "**ajax post**"
                data = request.POST['mydata']
                astr = data
                d = json.loads(data)
                with connection.cursor() as cursor:
                        try:
                                cursor.execute("select * from get_status_formalitie(%s)", [d['mcb']])
                                row = dictfetchall(cursor)
                                if row:
                                        print row
                                        return render(request, 'workflow/status_tracing_formalitie.html',{'formalitie':row})
                        finally:
                                cursor.close()
    return render(request)

def TracingformalitiesOffice (request):
        with connection.cursor() as cursor:
                try:
                        office_user = request.user.office
                        cursor.execute("select * from tracing_formalitie_officie(%s)", [office_user])
                        row = dictfetchall(cursor)
                        if row:
                                office = Offices.objects.filter(id=office_user)
                                return render(request, 'workflow/tracing_formalitie.html',{'formalities':row,'office':office[0].name})
                finally:
                        cursor.close()

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def generatePDF(request):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'
    p = canvas.Canvas(response)
    p.drawString(100, 100, "Hello world.")
    p.showPage()
    p.save()
    return response

from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib.units import cm
from reportlab.lib import colors

class ReportePersonasPDF(View):  
     
        def cabecera(self,pdf):
                archivo_imagen = settings.MEDIA_ROOT+'/images/cabecera.png'
                #x,y,size,y
                pdf.drawImage(archivo_imagen, 24, 730, 550, 100,preserveAspectRatio=True)                
        def tabla(self,pdf,y):
                #Creamos una tupla de encabezados para neustra 
                encabezados = ('id', 'name', 'state')
                #Creamos una lista de tuplas que van a contener a las personas
                detalles = [(persona.id, persona.name, persona.state) for persona in Offices.objects.all()]
                #Establecemos el tamaño de cada una de las columnas de la tabla
                detalle_orden = Table([encabezados] + detalles, colWidths=[2 * cm, 5 * cm, 5 * cm])
                #Aplicamos estilos a las celdas de la tabla
                detalle_orden.setStyle(TableStyle(
                [
                #La primera fila(encabezados) va a estar centrada
                ('ALIGN',(0,0),(2,0),'CENTER'), 
                #Los bordes de todas las celdas serán de color negro y con un grosor de 1
                ('GRID', (0, 0), (-1, -1), 1, colors.black), 
                #El tamaño de las letras de cada una de las celdas será de 10
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ]
                ))
                #Establecemos el tamaño de la hoja que ocupará la tabla 
                detalle_orden.wrapOn(pdf, 800, 600)
                #Definimos la coordenada donde se dibujará la tabla
                detalle_orden.drawOn(pdf, 60,200)
        
        def get(self, request, *args, **kwargs):
                #Indicamos el tipo de contenido a devolver, en este caso un pdf
                response = HttpResponse(content_type='application/pdf')
                #La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
                buffer = BytesIO()
                #Canvas nos permite hacer el reporte con coordenadas X y Y
                pdf = canvas.Canvas(buffer)
                #Llamo al método cabecera donde están definidos los datos que aparecen en la cabecera del reporte.
                self.cabecera(pdf)
                y = 600
                self.tabla(pdf, y)
                #Con show page hacemos un corte de página para pasar a la siguiente
                pdf.drawString(220, 730, "INFORME TECNICO - EMAP")
                pdf.drawString(80, 700, "A:")
                pdf.drawString(80, 650, "VIA:")
                pdf.drawString(80, 600, "DE:")
                pdf.drawString(80, 550, "REF:")
                pdf.drawString(80, 500, "FECHA:")
                pdf.drawString(90, 470, "1.    ANTECEDENTES")
                pdf.drawString(80, 450, "En cumplimiento a la Legislación ambiental Nº 1333 en su artículo 25 menciona “(…)")
                pdf.drawString(80, 435, "deben contar obligatoriamente con la identificación de la categoría de evaluación de impacto ambiental ") 
                pdf.showPage()
                pdf.drawString(100, 500, "1.    ANTECEDENTES")
                pdf.save()
                pdf = buffer.getvalue()
                buffer.close()
                response.write(pdf)
                return response

from io import BytesIO
from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import SimpleDocTemplate, Paragraph,Image,Spacer
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER,TA_LEFT,TA_JUSTIFY
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.platypus.flowables import HRFlowable
import os
from rotatedtext import verticalText
        
class MyPrint:


    def __init__(self, buffer, pagesize):
        self.buffer = buffer
        if pagesize == 'A4':
            self.pagesize = A4
        elif pagesize == 'Letter':
            self.pagesize = letter
        self.width, self.height = self.pagesize

    #@staticmethod
    def _header_footer(self,canvas, doc):
        canvas.saveState()
        styles = getSampleStyleSheet()
        sp = ParagraphStyle('parrafos',
                            alignment=TA_CENTER,
                            fontSize=14,
                            fontName="Times-Roman")
        try:
                archivo_imagen = os.path.join(settings.MEDIA_ROOT, str('images/cabecera.png'))
                imagen = Image(archivo_imagen, width=560, height=70, hAlign='RIGHT')
        except:
                imagen = Paragraph(u"LOGO", sp)
        encabezado = [[imagen]]
        tabla_encabezado = Table(encabezado, colWidths=[18 * cm])
        style = TableStyle(
            [
                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ]
        )
        tabla_encabezado.setStyle(style)
        tabla_encabezado.wrapOn(canvas, 50, 710)
        tabla_encabezado.drawOn(canvas, 50, 710)


        try:
                image_footer = os.path.join(settings.MEDIA_ROOT, str('images/piepagina.png'))
                imagen = Image(image_footer, width=560, height=70, hAlign='RIGHT')
        except:
                imagen = Paragraph(u"LOGO", sp)
        footer = [[imagen]]
        table_footer = Table(footer, colWidths=[18 * cm])
        style = TableStyle(
            [
                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ]
        )
        table_footer.setStyle(style)
        table_footer.wrapOn(canvas, 50, 5)
        table_footer.drawOn(canvas, 50, 5)

        #footer = Paragraph('This is a multi-line footer.  It goes on every page.   ' * 5, styles['Normal'])
        #w, h = footer.wrap(doc.width, doc.bottomMargin)
        #footer.drawOn(canvas, doc.leftMargin, h)

        canvas.restoreState() 
        
#----------------------------------------------------------------------------------------------

    def print_report(self,to,track,of):#aqui aumentamos las variables que llegan desde el formulario
        buffer = self.buffer
        doc = SimpleDocTemplate(buffer,
                                rightMargin=50,
                                leftMargin=50,
                                topMargin=75,
                                bottomMargin=80,
                                pagesize=self.pagesize)
                                #pagesize=self.pagesize, showBoundary=True)
 

        # Our container for 'Flowable' objectss
        elements = []
 
        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
        styleTitle=ParagraphStyle(name='centered',fontSize = 10,alignment = TA_CENTER)
        styleNormalJustify=ParagraphStyle(name='normal',fontSize = 10,alignment = TA_JUSTIFY)
        styleNormal=ParagraphStyle(name='normal',fontSize = 10,alignment = TA_LEFT)
        # Draw things on the PDF. Here's where the PDF generation happens.
        # See the ReportLab documentation for the full list of functionality.
        # Make heading for each column and start data list
        # Make heading for each column and start data list
        column1Heading = Paragraph("CONSULTORES AMBIENTALES",styleNormal)
        column2Heading = Paragraph("C.I.",styleNormal)
        column3Heading = Paragraph("N RENCA",styleNormal)
        column4Heading = Paragraph("VIGENCIA",styleNormal)
        column5Heading = Paragraph("OBSERVACIONES",styleNormal)
        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading,column4Heading,column5Heading])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        dataReqGenerals.append([Paragraph('Ing. del Medio Ambiente Rubén Vladimir Ari Torrez',styleNormal),'3977159 PT.','153552','30.07.2019',Paragraph('Para la 2da presentación deberá adjuntar una fotostática del CI',styleNormal)])
        tableReqGenerals = Table(dataReqGenerals, [6* cm, 3 * cm,3* cm, 3 * cm,4*cm], repeatRows=1)
        tableReqGenerals.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                        ('TEXTCOLOR',(1,1),(-2,-2),colors.red),
                        ('VALIGN',(0,0),(0,-1),'TOP'),
                        ('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                        ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        ('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        #tblStyle.add('BACKGROUND',(0,0),(1,0),colors.lightblue)
        #tblStyle.add('BACKGROUND',(2,0),(3,0),colors.lightblue)
        #tblStyle.add('BACKGROUND',(0,1),(-1,-1),colors.white)
        tableReqGenerals.setStyle(tblStyle)

        #LISTA DE VERIFICACION---------------------------------------------------------------------
        column1Heading = Paragraph("DOCUMENTO",styleNormal)
        column2Heading = Paragraph("CUMPLE",styleNormal)
        column3Heading = Paragraph("",styleNormal)
        column4Heading = Paragraph("OBSERVACIONES",styleNormal)
        
        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading,column4Heading])
        dataReqGenerals.append(["","SI","NO",""])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        #dataReqGenerals.append([Paragraph('',styleNormal),Paragraph('SI',styleNormal),Paragraph('NO',styleNormal),Paragraph('',styleNormal)])
        dataReqGenerals.append([Paragraph('Testimonio Público de Constitución de la Empresa',styleNormal),'X','-',Paragraph('-No presenta deberá adjuntar el Testimonio Publico de Constitución de EMPRESA MINERA INTI WASI S.R.L.',styleNormal)])
        dataReqGenerals.append([Paragraph('Testimonio notariado de otorgamiento de poder del representante legal.',styleNormal),'X','-',Paragraph('-Presenta, Testimonio 735/2018 del Distrito Judicial de: Potosí - Bolivia, Notaria de fe Publica N° Seis. Notario: Dra. Eunice Karen Cruz Saavedra. Testimonio de: Revocatoria total del Poder General y de Administración N° 524/2012 elaborado por ante la notaria de fe Publica número 11 a cargo del Dr. Héctor Baptista Revilla de Fecha 02 de agosto de 2012 del Distrito Judicial de Potosí – Bolivia, que otorgaron los Señores: Humberto Escurra Mollinedo y Olga Marily Escurra Daza en su condición de socios de socios de la Empresa Minera Inti Wasi S.R.L. En Favor del Señor Humberto Escurra Mollinedo y nuevo Otorgamiento de poder General de Administración representación Legal que suscriben los socios de la Empresa Minera Inti Wasi S.R.L. Señores Humberto Escurra Mollinedo y Ruth Daza Terceros de Escurra a Favor del socio: Humberto Escurra Mollinedo. Lugar y Fecha: Potosí, 30 de Octubre de 2018.-Presenta, Testimonio 885/2018 del Distrito Judicial de: Potosí - Bolivia, Notaria de fe Publica N° Seis. Notario: Eunice K. Cruz Saavedra. Testimonio de: Escritura Pública de una Transferencia de cuotas de capital en sociedad de Responsabilidad Limitada que suscriben los señores: Humberto Escurra Mollinedo, Olga Marily Escurra Daza y Ruth Daza Terceros de Escurra. Fecha: Potosí, 29 de Octubre de 2018.',styleNormal)])
        dataReqGenerals.append([Paragraph('Contrato de Arrendamiento o Autorización Transitoria Especial de otorgación de derechos (cuadriculas/pertenencia)',styleNormal),'X','-',Paragraph('-No presenta Contrato de arrendamiento o Autorización Transitoria especial de otorgación de Derechos (cuadriculas/pertenencia)-Al ser un contrato pre-constituido, complemente con el certificado de vigencia emitido por la AJAM del área Minera o similar.',styleNormal)])
        dataReqGenerals.append([Paragraph('Derecho Propietario del área de emplazamiento del proyecto',styleNormal),'X','-',Paragraph('-No presenta inscripción en derechos r',styleNormal)])
        #elements.append(Spacer(1,0.2*inch))
        dataReqGenerals.append([Paragraph('Certificado de Área Libre emitido por la AJAM (Art 44 de RM Nº 023/2015)',styleNormal),'X','-',Paragraph('NC',styleNormal)])
        dataReqGenerals.append([Paragraph('Resolución del Prosecución de Tramite  (Art 45 de RM Nº 023/2015)',styleNormal),'X','-',Paragraph('NC',styleNormal)])
        dataReqGenerals.append([Paragraph('Copia de la Presentación, revisión y aprobación de los planes de trabajo y presupuesto financiero (Art 27 de RM Nº 023/2015)',styleNormal),'X','-',Paragraph('NC',styleNormal)])
        dataReqGenerals.append([Paragraph('Resolución Administrativa de otorgación de licencia de prospección y exploración o prospección aérea. (Art 46 de RM Nº 023/2015)',styleNormal),'X','-',Paragraph('NC',styleNormal)])
        dataReqGenerals.append([Paragraph('Protocolización de Contrato de Arrendamiento o Autorización Transitoria de otorgación de derechos (cuadriculas/pertenencia) por la AJAM.',styleNormal),'X','-',Paragraph('NC',styleNormal)])
        dataReqGenerals.append([Paragraph('Fotocopia de Cédula de identidad del representante legal.',styleNormal),'X','-',Paragraph('Ninguna, N° 1362737 Pt.',styleNormal)])
        dataReqGenerals.append([Paragraph('Certificado NIT y certificación electrónica',styleNormal),'X','-',Paragraph('-N° 171140022. Contribuyente: EMPRESA MINERA INTI WASI S.R.L. Actividad Principal: 40505 – Comercialización y Actividad (s) Secundaria (s): Explotación (40502). Prospección y Exploración (40501) -Deberá actualizar la fecha de la certificación lo que presento es de la anterior gestión.',styleNormal)])
        dataReqGenerals.append([Paragraph('Registro de Comercio de Bolivia. (FUNDEMPRESA)',styleNormal),'X','-',Paragraph('-Certificado de Registro de Comercio de Bolivia FUNDEMPRESA N° Registró 00169903 vigente hasta 28.02.2019. -Deberá Actualizar Registro de Comercio de Bolivia. (FUNDEMPRESA) ya no se encuentra vigente.',styleNormal)])
        dataReqGenerals.append([Paragraph('Número de identificación Minera NIM (SENARECOM).',styleNormal),'X','-',Paragraph('-Presenta con Nº 05-0196-04 Vigente hasta el 27/08/2019.Departamento de Potosí, Provincia: Sud Chichas, Municipio: Tupiza',styleNormal)])
        dataReqGenerals.append([Paragraph('Certificado de Registro a sustancias controladas',styleNormal),'X','-',Paragraph('Complemente con la presentación del certificado de registro a la Dirección General de Sustancias Controladas para el manejo de los insumos peligrosos.',styleNormal)])
        dataReqGenerals.append([Paragraph('Certificado de Registro emitido por Servicio Nacional de Geología  y Técnico de Minas (exploración y explotación).',styleNormal),'X','-',Paragraph('-No Presenta, deberá adjuntar la misma a ser un contrato pre constituido, o deberá presentar el la información de datos de certificación de vigencia de contrato del área minera emitido por la AJAM.',styleNormal)])
        dataReqGenerals.append([Paragraph('Formulario de pago de patente minera de AJAM (Actualizado)',styleNormal),'X','-',Paragraph('Presenta pago de patentes de la gestión 2018 denominación “Marcela”, Titular: Ruth Daza Terceros con el Código único N° 26118, extensión 3 cuadriculas. Debe presentar pago de patente de la gestión 2019, no coincide con la Representación Legal. Aclare.',styleNormal)])
        dataReqGenerals.append([Paragraph('Plano Definitivo y/o Plano Catastral de las concesiones mineras emitido por la AJAM.',styleNormal),'X','-',Paragraph('Presenta plano Definitivo de la Autoridad Jurisdiccional Administrativa Minera AJAM - Dirección de Catastro y Cuadriculado Minero DCCM. Nombre del Área Minera (Ex Concesión): Marcela. Actor Productivo Minero (Ex - Concesionario): Ruth Daza Terceros, no coincide con la Representación Legal. Aclare.',styleNormal)])
        dataReqGenerals.append([Paragraph('Licencia de Funcionamiento Municipal',styleNormal),'X','-',Paragraph('Licencia de Funcionamiento Municipal',styleNormal)])
        dataReqGenerals.append([Paragraph('Acta de convenio y/o conformidad con la comunidad de la concesión minera y/o la Empresa.',styleNormal),'X','-',Paragraph('No presenta deberá adjuntar la misma ',styleNormal)])
        dataReqGenerals.append([Paragraph('Plan de Contingencias (si corresponde).',styleNormal),'X','-',Paragraph('-No presenta deberá adjuntar el Testimonio PR.L.',styleNormal)])
        dataReqGenerals.append([Paragraph('Plan de manejo conjunto de las acumulaciones de menor volumen que se encuentren dentro del perímetro de ATE, art. 52 del RAAM',styleNormal),'X','-',Paragraph('-No presenta deberá adjuntar el Testimonio PuNTI WASI S.R.L.',styleNormal)])
        dataReqGenerals.append([Paragraph('Plan de Manejo de Residuos Sólidos y líquidos',styleNormal),'X','-',Paragraph('-No presenta deberá adjuntar el Testimonio PuRA INTI WASI S.R.L.',styleNormal)])
        dataReqGenerals.append([Paragraph('Anexo I RAAM Lista y Características de Sustancias Peligrosas que utilizan en la operación minera',styleNormal),'X','-',Paragraph('-No presenta deberá adjuntar el Testimonio PuNTI WASI S.R.L.',styleNormal)])
        dataReqGenerals.append([Paragraph('Plan de cierre y rehabilitación del área',styleNormal),'X','-',Paragraph('-No presenta deberá adjuntar el Testimonio PuA INTI WASI S.R.L.',styleNormal)])
        dataReqGenerals.append([Paragraph('Copia Digital del Formulario EMAP de la AOP (contenido de todo el documento de respaldo).',styleNormal),'X','-',Paragraph('-No presenta deberá adjuntar el Testimonio PuRA INTI WASI S.R.L.',styleNormal)])

        tableList = Table(dataReqGenerals, [7* cm, 1 * cm,1* cm, 8 * cm,4 * cm], repeatRows=1)
        tableList.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                        #('TEXTCOLOR',(1,1),(-2,-2),colors.red),
                        ('VALIGN',(0,0),(0,-1),'TOP'),
                        ('SPAN',(0,0),(0,1)),
                        ('SPAN',(1,0),(2,0)),
                        ('SPAN',(3,0),(3,1)),
                        #('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                        ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        #('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        #tblStyle.add('BACKGROUND',(0,0),(1,0),colors.lightblue)
        #tblStyle.add('BACKGROUND',(2,0),(3,0),colors.lightblue)
        #tblStyle.add('BACKGROUND',(0,1),(-1,-1),colors.white)
        tableList.setStyle(tblStyle)
        #----------------------------------------------------------------------------
        column1Heading = Paragraph("CRITERIOS TECNICOS",styleNormal)
        column2Heading = Paragraph("CUMPLE",styleNormal)
        column3Heading = Paragraph("",styleNormal)
        column4Heading = Paragraph("OBSERVACIONES",styleNormal)
        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading,column4Heading])
        dataReqGenerals.append(["","SI","NO",""])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        #dataReqGenerals.append([Paragraph('',styleNormal),Paragraph('SI',styleNormal),Paragraph('NO',styleNormal),Paragraph('',styleNormal)])
        dataReqGenerals.append([Paragraph('1. DATOS GENERALES (A.O.P)',styleNormal)])
        dataReqGenerals.append([Paragraph('1.1 Nombre del Proyecto',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('1.2 Titulo de la concesión minera',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('1.3 Registro (a ser llenado por la SNM)',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('2 EXPLORACION',styleNormal)])
        dataReqGenerals.append([Paragraph('2.1 Área de Exploración',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3. ACTIVIDADES MINERAS MENORES DE IMPACTOS AMBIENTALES CONOCIDOS NO SIGNIFICATIVOS AMIAC',styleNormal)])
        dataReqGenerals.append([Paragraph('3.1	Área de la AMIAC',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.2 Descripción general de las operaciones, del área y facilidades de la AMIAC',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.2.1 Presentar una descripción general de las operaciones y adjuntar plano en el que se identifiquen caminos y sendas de acceso (existentes y a construirse), pistas, helipuertos y puentes, residuos mineros, boca minas, e instalaciones de concentración de minerales (depósitos y presas de colas), infraestructura (campamentos y servicios sanitarios, almacenes, depósitos generales y de combustibles, polvorines, talleres, tanques de agua), fuentes de energía (grupo electrógeno, línea de alta tensión y transformador, instalación hidroeléctrica, motores a combustión, otros), ríos, quebradas, lagos, lagunas, vertientes y pozos.',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.2.2 AUDITORIA DE LÍNEA BASE',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.2.2.1 Fuentes de contaminación e infraestructura existentes',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.2.2.2 Aguas superficiales',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.2.2.3 Aguas subterráneas',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.2.2.4 Suelos y Vegetación',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.3 Actividades de minería',styleNormal)])
        dataReqGenerals.append([Paragraph('3.3.1 Reconocimiento, desarrollo y preparación',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.3.2 Arranque y Extracción',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.3.3 Tipo de Minería',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.3.4 Sustancias Peligrosas',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.4 Actividades de concentración y beneficio',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.5 Residuos',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('3.6 Plazos de adecuación: (para AMIAC existentes a la fecha de vigencia del RAAM)',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('4. DECLARACION JURADA Y FIRMAS',styleNormal)])
        dataReqGenerals.append([Paragraph('Nombres, firmas y número de cédula de identidad, promotor y responsable técnico.',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])


        tableList2 = Table(dataReqGenerals, [7* cm, 1 * cm,1* cm, 8 * cm,4 * cm], repeatRows=1)
        tableList2.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                        #('TEXTCOLOR',(1,1),(-2,-2),colors.red),
                        ('VALIGN',(0,0),(0,-1),'TOP'),
                        ('SPAN',(0,0),(0,1)),
                        ('SPAN',(1,0),(2,0)),
                        ('SPAN',(3,0),(3,1)),
                        ('SPAN',(3,2),(0,2)),
                        ('SPAN',(3,6),(0,6)),
                        ('SPAN',(3,8),(0,8)),
                        ('SPAN',(3,17),(0,17)),
                        ('SPAN',(3,25),(0,25)),
                        #('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                        ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        #('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        tableList2.setStyle(tblStyle)
        #----------------------------------------------------------------------------
        column1Heading = Paragraph("DOCUMENTO",styleNormal)
        column2Heading = Paragraph("CUMPLE",styleNormal)
        column3Heading = Paragraph("",styleNormal)
        column4Heading = Paragraph("OBSERVACIONES",styleNormal)
        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading,column4Heading])
        dataReqGenerals.append(["","SI","NO",""])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        #dataReqGenerals.append([Paragraph('',styleNormal),Paragraph('SI',styleNormal),Paragraph('NO',styleNormal),Paragraph('',styleNormal)])
        dataReqGenerals.append([Paragraph('Resumen ejecutivo',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('Planos constructivos',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('1.3 Registro (a ser llenado por la SNM)',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('Mapas de ubicación del proyecto',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('Imagen satelital de ubicación del proyecto debidamente georeferenciada',styleNormal)])
        dataReqGenerals.append([Paragraph('Mapa vial de acceso al área del proyecto',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
        dataReqGenerals.append([Paragraph('Reporte fotográfico del proyecto',styleNormal),'X','-',Paragraph('-----------------',styleNormal)])
  
        tableList3 = Table(dataReqGenerals, [7* cm, 1 * cm,1* cm, 8 * cm,4 * cm], repeatRows=1)
        tableList3.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                        #('TEXTCOLOR',(1,1),(-2,-2),colors.red),
                        ('VALIGN',(0,0),(0,-1),'TOP'),
                        ('SPAN',(0,0),(0,1)),
                        ('SPAN',(1,0),(2,0)),
                        ('SPAN',(3,0),(3,1)),
                       # ('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                        ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                       # ('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        tableList3.setStyle(tblStyle)
        #--------------------------------------------------------------------------------\
        #TABLELIST4
        column1Heading = Paragraph("N°",styleNormal)
        column2Heading = Paragraph("OBSERVACION",styleNormal)
        column3Heading = Paragraph("ACLARACION/ COMPLEMENTACION/ ENMIENDA",styleNormal)
        column4Heading = Paragraph("PAG.",styleNormal)
        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading,column4Heading])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        dataReqGenerals.append([Paragraph('1',styleNormal),'-','-',Paragraph('-----',styleNormal)])

        tableList4 = Table(dataReqGenerals, [1* cm, 4 * cm,9* cm, 2 * cm,4 * cm], repeatRows=1)
        tableList4.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                       # ('TEXTCOLOR',(1,1),(-2,-2),colors.red),
                        ('VALIGN',(0,0),(0,-1),'TOP'),
                        #('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                        ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        #('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        tableList4.setStyle(tblStyle)
        #-------------------------------------------------------------------------------

        dataPersons = []        
        dataPersons.append(['A:',to])#aqui ponemos los campos que llegan
        dataPersons.append(['','SECRETARIO DEPARTAMENTAL  DE LA MADRE TIERRA'])
        dataPersons.append(['',''])
        dataPersons.append(['VIA:',track])
        dataPersons.append(['','JEFE DE UNIDAD GESTIÓN AMBIENTAL Y USO DE RECURSOS NATURALES'])
        dataPersons.append(['',''])
        dataPersons.append(['DE:',of])
        dataPersons.append(['','GESTION AMBIENTAL MACRO REGION NORTE'])
        dataPersons.append(['',''])
        dataPersons.append(['REF','REVISIÓN: FORMULARIO EMAP: “ÁREA MINERA MARCELA – EMPRESA MINERA\nINTI WASI S.R.L.”'])
        dataPersons.append(['',''])
        dataPersons.append(['FECHA:','Potosí, 21 de Marzo de 2019.'])
        tablePersons = Table(dataPersons,[3*cm,15*cm],repeatRows=1)
        tablePersonsStyle = TableStyle([
                        ('ALIGN',(0,-1),(-1,-1),'LEFT'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.white),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.white),])
        tablePersons.setStyle(tablePersonsStyle)
#---------------------------------------------------------------------------------------
        column1Heading = Paragraph("PUNTO",styleNormal)
        column2Heading = Paragraph("Coord. UTM Puntos Referencia y Partida  (Zona 20)",styleNormal)
        column3Heading = Paragraph("---",styleNormal)

        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        dataReqGenerals.append([Paragraph('-',styleNormal),'NORTE','ESTE'])
        dataReqGenerals.append([Paragraph('1',styleNormal),'7627000','234500'])
        dataReqGenerals.append([Paragraph('2',styleNormal),'7627000','234500'])
        dataReqGenerals.append([Paragraph('3',styleNormal),'7627000','234500'])
        dataReqGenerals.append([Paragraph('4',styleNormal),'7627000','234500'])
        dataReqGenerals.append([Paragraph('5',styleNormal),'7627000','234500'])
        dataReqGenerals.append([Paragraph('6',styleNormal),'7627000','234500'])


        tableCC = Table(dataReqGenerals, [2* cm, 3 * cm,3* cm,4 * cm], repeatRows=1)
        tableCC.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                        ('SPAN',(1,0),(2,0)),
                        ('SPAN',(0,0),(0,1)),
                        ('VALIGN',(0,0),(0,1),'CENTER'),
                        #('VALIGN',(0,0),(0,-1),'TOP'),
                        #('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                        #('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                       # ('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        tableCC.setStyle(tblStyle)
#------------------------------------------------
#---------------------------------------------------------------------------------------
        column1Heading = Paragraph("Implementación y Ejecución de la Medidas de Mitigación",styleNormal)
        column2Heading = Paragraph("",styleNormal)
        column3Heading = Paragraph("",styleNormal)
        column4Heading = Paragraph("",styleNormal)
        column5Heading = Paragraph("",styleNormal)
        column6Heading = Paragraph("",styleNormal)
        column7Heading = Paragraph("",styleNormal)
        column8Heading = Paragraph("",styleNormal)
        column9Heading = Paragraph("",styleNormal)
        column10Heading = Paragraph("",styleNormal)
        column11Heading = Paragraph("",styleNormal)
        column12Heading = Paragraph("",styleNormal)
        column13Heading = Paragraph("",styleNormal)
        column14Heading = Paragraph("Observaciones",styleNormal)

        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading,column4Heading,column5Heading,column6Heading,column7Heading,column8Heading,column9Heading,column10Heading,column11Heading,column12Heading,column13Heading,column14Heading])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        dataReqGenerals.append([verticalText('Número'),verticalText('Código'),verticalText('Factor Ambiental'),verticalText('Ítem'),verticalText('Impacto Ambiental'),verticalText('Medias de Mitigación'),verticalText('Metodología'),verticalText('Ubicación'),verticalText('Unidad'),verticalText('Cantidad'),verticalText('Precio Unitario'),verticalText('Total'),verticalText('Responsable')])
        dataReqGenerals.append([Paragraph('',styleNormal),'',''])

        table1R = Table(dataReqGenerals, [1* cm, 1 * cm,1* cm, 1* cm, 1 * cm,1* cm, 1* cm, 1 * cm,1* cm, 1* cm, 1 * cm,1* cm, 1* cm, 3 * cm, 4 * cm], repeatRows=1)
        table1R.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                        ('VALIGN',(0,0),(0,-1),'TOP'),
                        ('SPAN',(0,0),(12,0)),
                        ('VALIGN',(0,0),(13,1),'CENTER'),
                        ('SPAN',(13,0),(13,1)),
                        ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        table1R.setStyle(tblStyle)
#------------------------------------------------
        column1Heading = Paragraph("Seguimiento a la Implementación de la Medidas de Mitigación",styleNormal)
        column2Heading = Paragraph("",styleNormal)
        column3Heading = Paragraph("",styleNormal)
        column4Heading = Paragraph("",styleNormal)
        column5Heading = Paragraph("",styleNormal)
        column6Heading = Paragraph("",styleNormal)
        column7Heading = Paragraph("",styleNormal)
        column8Heading = Paragraph("",styleNormal)
        column9Heading = Paragraph("",styleNormal)
        column10Heading = Paragraph("",styleNormal)
        column11Heading = Paragraph("",styleNormal)
        column12Heading = Paragraph("",styleNormal)
        column13Heading = Paragraph("",styleNormal)
        column14Heading = Paragraph("Observaciones",styleNormal)

        # Assemble data for each column using simple loop to append it into data list
        dataReqGenerals = []
        dataReqGenerals.append([column1Heading,column2Heading,column3Heading,column4Heading,column5Heading,column6Heading,column7Heading,column8Heading,column9Heading,column10Heading,column11Heading,column12Heading,column13Heading,column14Heading])
        #for i in range(1,100):
                #data.append([str(i),str(i),str(i),str(i)])
        dataReqGenerals.append([verticalText('Número'),verticalText('Código'),verticalText('Factor Ambiental'),verticalText('Ítem'),verticalText('Impacto Ambiental'),verticalText('Medias de Mitigación'),verticalText('Ubicación del Punto de '),verticalText('Parámetros de '),verticalText('Límites Permisibles'),verticalText('Frecuencia de Muestreo'),verticalText('Material Requerido'),verticalText('Costo del Seguimiento'),verticalText('Responsable')])
        dataReqGenerals.append([Paragraph('',styleNormal),'',''])

        table2R = Table(dataReqGenerals, [1* cm, 1 * cm,1* cm, 1* cm, 1 * cm,1* cm, 1* cm, 1 * cm,1* cm, 1* cm, 1 * cm,1* cm, 1* cm, 3 * cm, 4 * cm], repeatRows=1)
        table2R.hAlign = 'CENTER'
        tblStyle = TableStyle([('ALIGN',(1,1),(-2,-2),'CENTER'),
                        ('VALIGN',(0,0),(0,-1),'TOP'),
                        ('SPAN',(0,0),(12,0)),
                        ('VALIGN',(0,0),(13,1),'CENTER'),
                        ('SPAN',(13,0),(13,1)),
                        ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black),])
        table2R.setStyle(tblStyle)
#------------------------------------------------
        dataIUDP = []        
        dataIUDP.append(['Razón Social:','EMPRESA MINERA INTI WASI S.R.L.'])
        dataIUDP.append(['Sector:','Minería'])
        dataIUDP.append(['Sub Sector:','Industria minera'])
        dataIUDP.append(['Actividad Específica:','Extracción de minerales Zinc (15 %), Plomo (5 %)'])
        dataIUDP.append(['Representante Legal:','Humberto Escurra Mollinedo identificado con C.I. 1362737 Pt.'])
        dataIUDP.append(['Departamento:','Potosí'])
        dataIUDP.append(['Provincia/Municipio:','Sud chichas/ Tupiza'])
        dataIUDP.append(['Comunidad/Cantón:','Cantón Suipacha '])
        dataIUDP.append(['Coordenadas N, E/vértices ATE','Coordenadas UTM – Hoja Cartográfica Esc. 1 – 50.000  6429 - I '])

        tableIUDP = Table(dataIUDP,[5*cm,7*cm],repeatRows=1)
        tableIUDPStyle = TableStyle([
                        ('ALIGN',(0,-1),(-1,-1),'LEFT'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.white),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.white),])
        tableIUDP.setStyle(tableIUDPStyle)

        dataCM = []        
        dataCM.append(['Nombre de la ATE:','MARCELA'])
        dataCM.append(['Código Catastral/ Nº Registro :','508-06488'])
        dataCM.append(['Carta Nacional:','6429 – I Tupiza '])
        dataCM.append(['Superficie de la ATE:','3 cuadriculas'])
        dataCM.append(['Superfície:','75 Ha'])
        tableCM = Table(dataCM,[5*cm,7*cm],repeatRows=1)
        tableCMStyle = TableStyle([
                        ('ALIGN',(0,-1),(-1,-1),'LEFT'),
                        ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.white),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.white),])
        tableCM.setStyle(tableCMStyle)

        elements.append(Paragraph('INFORME TECNICO - EMAP', styleTitle))
        elements.append(Paragraph('SDMT - UGA 300/2019', styleTitle))
        elements.append(Spacer(1,0.2*inch))#para dar saltos de linea
        elements.append(tablePersons)
        elements.append(Spacer(1,0.2*inch))#para dar saltos de linea
        elements.append(HRFlowable(width='200%', thickness=0.5, color=colors.black))
        elements.append(Spacer(1,0.2*inch))#para dar saltos de linea
        elements.append(Paragraph('1. ANTECEDENTES', styleNormal))
        elements.append(Paragraph('En cumplimiento a la Legislación ambiental Nº 1333 en su artículo 25 menciona “(…) deben contar obligatoriamente con la identificación de la categoría de evaluación de impacto ambiental (…)”, asimismo el Reglamento Ambiental para Actividades Mineras DS 24782 establece en su Título XI, Capítulo III (Del Certificado de Dispensación Categoría 3 para Exploración y Actividades Mineras Menores) en sus artículos 118, 119 y 120 menciona “el concesionario u operador minero  presentara ante la Prefectura  del Departamento de Potosí, el Formulario EMAP debidamente llenado”.', styleNormalJustify))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('-En fecha 28.02.2019 el representante legal de la EMPRESA MINERA INTI WASI S.R.L. el señor Humberto Escurra Mollinedo, presenta por primera vez ante la Autoridad Ambiental Competente Departamental (SDMT) el formulario EMAP proyecto en operación: “ÁREA MINERA MARCELA”, la misma con hoja de ruta 241 instruye su evaluación y emisión de informe técnico conforme a normativa sectorial vigente', styleNormalJustify))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('2. OBJETIVO DEL PROYECTO', styleNormal))
        elements.append(Paragraph('Realizar actividades mineras de explotación con Actividades Mineras con Impactos Ambientales Conocidos no significativos en la ÁREA MINERA MARCELA.', styleNormalJustify))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('3. REQUISITOS GENERALES', styleNormal))
        elements.append(Spacer(1,0.2*inch))
        elements.append(tableReqGenerals)
        elements.append(Spacer(1,0.1*inch))
        elements.append(Paragraph('4. IDENTIFICACIÓN Y UBICACIÓN DEL PROYECTO', styleNormal))
        elements.append(Spacer(1,0.2*inch))
        elements.append(tableIUDP)
        elements.append(Spacer(1,0.2*inch))
        elements.append(tableCC)
        elements.append(Paragraph('Domicilio oficial, teléfonos:',styleNormal))
        elements.append(Paragraph('correo electrónico:', styleNormal))
        elements.append(Paragraph('4.1 TITULO DE LA CONCESION MINERA', styleNormal))
        elements.append(tableCM)
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('4.2 UBICACION ESPACIAL DE LA ATE', styleNormal))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('5. REVISION DEL DOCUMENTO', styleNormal))
        elements.append(Paragraph('La revisión del IRAP, ha sido desarrollada de acuerdo al Reglamento Ambiental para Actividades Mineras y criterios técnicos de acuerdo a legislación ambiental y reglamentos conexos.', styleNormalJustify))
        elements.append(Spacer(1,0.4*inch))
        elements.append(Spacer(1,0.4*inch))
        elements.append(Paragraph('LISTA DE VERIFICACION 1: CUMPLIMIENTO DE REQUISITOS ADMINISTRATIVOS Y LEGALES', styleTitle))
        elements.append(Spacer(1,0.1*inch))
        elements.append(tableList)
        elements.append(Paragraph('LISTA DE VERIFICACION 2 CUMPLIMIENTO ANEXO 2- RAAM', styleTitle))
        elements.append(Spacer(1,0.2*inch))
        elements.append( tableList2)     
        elements.append(Paragraph('Lista de verificación 3: Información técnica de respaldo', styleTitle))
        elements.append(Spacer(1,0.2*inch))
        elements.append( tableList3)   
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('OBSERVACIONES', styleNormal))
        elements.append(Paragraph('-	Corrija de su descripción refiere el término de “Concesión” corregir en todo el documento a Autorización Transitoria Especial (ATE) conforme al DS 726 con lo referido ', styleNormalJustify))
        elements.append(Paragraph('-	No presenta el Acta de visita de campo del consultor Ambiental que elaboro el Formulario EMAP y la misma debe estar firmada por autoridades del lugar del proyecto.', styleNormalJustify))
        elements.append(Paragraph('-	Deberá actualizar el Acta de convenio y conformidad con comunidades aledañas del área Minera solo de respaldo y evitar futuros conflictos. ', styleNormalJustify))
        elements.append(Paragraph('-	Complemente la descripción de las observaciones realizadas y mejore la explicación del formulario EMAP', styleNormalJustify))
        elements.append(Paragraph('-	Complemente con un anexo específico de RESUMEN la identificación de los impactos ambientales, Programa de Adecuación Ambiental y Plan de Aplicación y Seguimiento Ambiental respecto a los impactos ambientales considerado en la actividad minera, correspondiente a  los factores Aire, Agua, Suelo, Ruido, Seguridad e Higiene, Riesgos, sustancias peligrosas con el siguiente resumen:', styleNormalJustify))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('Tabla 1: Resumen Identificación de Impactos y Medidas de Mitigación', styleTitle))
        elements.append(Spacer(1,0.2*inch))
        elements.append( table1R)
        elements.append(Spacer(1,0.2*inch)) 
        elements.append(Paragraph('Tabla 2: Resumen Plan de Aplicación y Seguimiento Ambiental', styleTitle))
        elements.append( table2R)
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('6.	CONCLUSIONES ', styleNormal))
        elements.append(Paragraph('De acuerdo a la evaluación realizada del formulario EMAP del Proyecto en operación: “ÁREA MINERA MARCELA”, presentado por el Sr. Humberto Escurra Mollinedo identificado con C.I. 1362737 Pt. Representante Legal de la EMPRESA MINERA INTI WASI S.R.L.; se concluye que el documento ambiental presentado NO CUMPLE con lo solicitado en la normativa ambiental del Reglamento Ambiental para Actividades Mineras RAAM, debiendo sin embargo complementar y subsanar la información solicitada y presentar nuevamente para su valoración respectiva.', styleNormal))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('7.	RECOMENDACIONES.', styleNormal))
        elements.append(Paragraph('En concordancia al RAAM del Título XI del Capítulo III (Del Certificado de Dispensación Categoría 3 para Exploración y Actividades Mineras Menores) refrendado en sus  artículos 118,119 y 120 se recomienda DEVOLVER el Formulario EMAP del PROYECTO: “ÁREA MINERA MARCELA”, presentado por el Sr. Humberto Escurra Mollinedo identificado con C.I. 1362737 Pt. Representante Legal de la EMPRESA MINERA INTI WASI S.R.L., para que vuelva a presentar el Formulario EMAP con las complementaciones solicitadas.', styleNormal))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('Al consultor Ambiental se recomienda tomar mayor interés en la Elaboración de los documentos IRAPs, puesto que deberá elaborar el documento en su propio formato y no realizar copia fiel de otros documentos ya valorados en esta instancia ambiental.  ', styleNormal))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('En tal sentido remito el presente informe a efectos que el representante legal realice las correcciones pertinentes en un plazo no mayor a 15 días hábiles de acuerdo a lo establecido en el DS 3549 de 02-05-2018, incluyendo un detalle acerca de las complementaciones, aclaraciones y enmiendas incorporadas al documento. INCLUYENDO UN DETALLE adjunto en el documento acerca de las complementaciones, aclaraciones y enmiendas incorporadas al documento.', styleNormal))
        elements.append( tableList4)
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('Es cuanto informo para fines consiguientes', styleNormal))
        elements.append(Spacer(1,0.2*inch))
        elements.append(Paragraph('Cc/arch./RCEV', styleNormal))
        elements.append(Paragraph('Adj. Documento Formulario EMAP evaluado.', styleNormal))
        


        elements.append(Spacer(1,0.2*inch))
        doc.build(elements, onFirstPage=self._header_footer, onLaterPages=self._header_footer)
 

def print_report(request):
        if request.method == 'POST':
                to = request.POST['to'] # aqui mandamos los campos del formulario
                track = request.POST['track'] # aqui mandamos los campos del formulario
                of = request.POST['of']
                print track 
                #d = json.loads(data)
                #print d
                #return HttpResponse('send')
                response = HttpResponse(content_type='application/pdf')
                buffer = BytesIO()
                report = MyPrint(buffer, 'Letter')
                pdf = report.print_report(to,track,of)# aqui mandamos las variables para generar el reporte con los campos
                pdf = buffer.getvalue()
                buffer.close()
                response.write(pdf)
                return response


class NumberedCanvas(canvas.Canvas):


    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []
 

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()
 

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)
 
 
    def draw_page_number(self, page_count):
        # Change the position of this to wherever you want the page number to be
        self.drawRightString(211 * mm, 15 * mm + (0.2 * inch),
                             "Page %d of %d" % (self._pageNumber, page_count))




if __name__ == '__main__':
    buffer = BytesIO()
     
    report = MyPrint(buffer, 'Letter')
    pdf = report.print_report()
    buffer.seek(0)
 
    with open('arquivo.pdf', 'wb') as f:
        f.write(buffer.read())