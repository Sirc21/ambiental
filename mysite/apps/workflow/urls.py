from django.conf.urls import *
from .views import *

urlpatterns = [
    url(r'^run-formalities-office',RunformalitiesOffice),
    url(r'^start-formalitie',startProcessFormalitie),
    url(r'^send-tracing-formalitie',sendProcessFormalitie),
    url(r'^continue-tracing-formalitie',continueTracingFormalitie),
    url(r'^finish-tracing-formalitie',finishTracingFormalitie),
    url(r'^tracing-formalitie',TracingformalitiesOffice),
    url(r'^status-tracing-formalitie',statusTracingFormalitie),
    #url(r'^generate-pdf',print_users),
    url(r'^print-report',print_report),#print-report esto es la ruta para enviar el formulario con ajax
    url(r'^reporte_personas_pdf/$',ReportePersonasPDF.as_view(), name="reporte_personas_pdf"),
]
