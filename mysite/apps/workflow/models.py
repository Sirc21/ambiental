# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

class Workflow(models.Model):
    formalitie_hash = models.CharField(max_length=200)
    office_id = models.IntegerField()
    order_hash = models.IntegerField()
    tracing = models.IntegerField(default=0)
    state = models.CharField(max_length=200,default='INICIADO')
    state_officie = models.CharField(max_length=200,default='NONE')
    user_id = models.IntegerField()
    user_assign = models.IntegerField()
    created_date = models.DateTimeField(
            default=timezone.now)

    def saveWorkflow(self):
        self.save()

    def __str__(self):
        return self.title